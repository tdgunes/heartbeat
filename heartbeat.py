"""
heartbeat.py: sends periodic updates to the so_web
"""
import os
import sys
import logging
import argparse
import importlib
import time
import requests


def set_logging_level(loglevel):
    importlib.reload(logging)
    logging.shutdown()
    numeric_level = getattr(logging, loglevel, None)

    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % loglevel)

    logging.basicConfig(level=numeric_level, format="[%(filename)s:%(lineno)s - %(funcName)s()]"
                                                    " <%(asctime)s> <{}>\n%(message)s".format(loglevel))


def main(args=None):
    parser = argparse.ArgumentParser(description="heartbeat.py")
    parser.add_argument("-l", "--log",
                        help="log level of heartbeat.py options: DEBUG, INFO, WARNING, ERROR, CRITICAL",
                        type=str,
                        default=os.environ.get("HEARTBEAT_LOG_LEVEL", "DEBUG"))
    parser.add_argument("-s", "--endpoint",
                        help="so_web API endpoint for sending heartbeats",
                        type=str,
                        default=os.environ.get("HEARTBEAT_ENDPOINT", "http://127.0.0.1:8000/api/vm/heartbeat"))

    parser.add_argument("-n", "--name",
                        help="Name of VM that is sending heartbeats",
                        type=str,
                        default=os.environ.get("HEARTBEAT_VM_NAME", "droplet"))

    parser.add_argument("-m", "--period",
                        help="The time period for sending heartbeats",
                        type=int,
                        default=os.environ.get("HEARTBEAT_PERIOD", 10))

    print("Sys args:", sys.argv)
    args = parser.parse_args(args=args)
    loglevel = args.log.upper()
    set_logging_level(loglevel)
    logging.info("Given arguments: " + str(args))

    while True:
        logging.debug("Heartbeats to {} about name: {}".format(args.endpoint, args.name))

        try:
            response = requests.post(args.endpoint, json={"name": args.name})
            if response.status_code != 200:
                logging.warning("Response code received is {}.".format(response.status_code))
        except requests.exceptions.ConnectionError:
            logging.error("Couldn't able to connect.")

        logging.debug("Waiting for {} second(s)...".format(args.period))
        time.sleep(args.period)


if __name__ == '__main__':
    main()
